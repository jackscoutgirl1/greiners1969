<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="promo-block" style="background-image: url('images/menu/promo-menu.png')">
    <span class="promo-text">Menu</span>
</div>

<div class="container main">
    <div class="dark-block title-dark-block">
        <h2>MAKE THE FOOD COME TO YOU. CALL 317.783.4136 FOR DELIVERY.</h2>
    </div>
    <div class="row menu-row">
        <div class="col">
            <div class="dark-block">
                <h3>Hot Subs</h3>
                <ul class="menu-list">
                    <li><span>OWEN’S PHILLY CHEESE STEAK</span> USDA steak.</li>
                    <li><span>TRUMAN&#8217;S MUSHROOM STEAK</span> add mushroom gravy for a little extra coin.</li>
                    <li><span>LUCY’S PIZZA STEAK</span> cheese, lettuce, onions, olives, banana peppers, mushrooms, tomatoes,  vinegar + oil, salt + pepper.</li>
                    <li><span>FIRE GUY’S BBQ CHICKEN</span> shredded chicken with a wonderful bbq finish.</li>
                    <li><span>MEAT HEADS MEATBALL</span> just really large meatballs covered in our locally famous marinara sauce + melted provolone.</li>
                    <li><span>NOT YOUR HOT POCKET STROMBOLI</span> fennel sausage, marinara sauce, onions, provolone, mozzarella cheese on sourdough.</li>
                    <li><span>THREE CHEESE GRILLED CHEESE</span> self explanatory all on two pieces of rather large Turano bread &#8211; an illegal amount of fun.</li>
                    <li><span>BRIDGET’S PIZZA BURGER</span> ground beef mixed with our locally famous marinara sauce and melted provolone cheese.</li>
                    <li><span>TOTINO’S PEPPERONI AND CHEESE</span> pepperoni slices with melted provolone cheese. marinara sauce too.</li>
                    <li><span>LUKE’S COUNTRY SAUSAGE &amp; CHEESE</span> ground pork sausage with melted provolone cheese.</li>
                    <li><span>AUDREY’S GYRO</span> yee-roh gyro seasoned beef and lamb with lettuce, tomato, onion and tsaziki sauce.</li>
                    <li><span>BAKER’S BLT</span> an illegal amount of chubby bacon with magnificent tomatoes and lettuce. let us know about the mayo.</li>
                    <li><span>MOTHER CLUCKER CHICKEN</span> seasoned grilled chicken breast with melted provolone cheese. ADD TERIYAKI FOR SOME EXTRA COINS.</li>
                    <li><span>MADELINE’S ITALIAN BEEF</span> the highest grade thin sliced roast beef, provolone cheese, giardiniera mix, dipped in hot beef au jus. the end.</li>
                    <li><span>MRS. CHANDLER’S GRILLED PEANUT BUTTER + JELLY</span> the first reference of peanut butter paired with jelly on bread to be published in the United States was by Julia Davis Chandler in 1901 in the Boston Cooking-School Magazine of Culinary Science and Domestic Economics, Mrs. Chandler would be pleased we think, with our choosing to grill it.</li>
                    <li><span>HOT TODDY</span> Our finest TRIPLE MEAT &#8211; turkey, ham, prosciutto, lettuce, tomato, banana peppers, topped with DOUBLE THE CHEESE &#8211; melted sharp cheddar cheese + pepper jack cheese and finished with a special Hot Toddy sauce involving strata, poblano, chipotle mayonnaise.</li>
                </ul>
                <h4>LOAD UP YOUR SANDWICH WITH YOUR CHOICE OF LETTUCE, ONION, TOMATO, PICKLE, BANANA PEPPERS, OLIVES, GIARDINERA AND JALENPENOS</h4>
                <h4>DOUBLE THE MEAT AND MAKE ANY SANDWICH JUMBO</h4>
                <h4>EXCHANGE BREAD FOR A WRAP ASK ABOUT YOUR GLUTEN FREE BREAD</h4>
            </div>
            <div class="dark-block">
                <h3>Cold Subs</h3>
                <ul class="menu-list">
                    <li><span>RAY&#8217;S 1969 HOAGIE</span> genoa salami, beef salami, lean ham and provolone cheese. we recommend that you get this fixed up “old school” which is lettuce, onion, tomato, pepperoncini, oregano, oil + vinegar, salt and pepper.</li>
                    <li><span>MARY&#8217;S CHICKEN SALAD</span> lovely chicken breasts, red onion, celery, our special seasonings with a secret mayo.</li>
                    <li><span>TINA TUNA</span> named after our beloved Tina that has worked here since 1970 something. albacore tuna mixed with a deluxe mayo, celery, red onion + special seasoning.</li>
                    <li><span>CLAIRE&#8217;S VEGGIE HEAD</span> cheese, lettuce, onions, olives , banana peppers, mushrooms, tomatoes, cucumbers, vinegar + oil, salt + pepper.</li>
                    <li><span>MR. TURKEY</span> turkey with provolone &#8211; not complicated. adding bacon is extra.</li>
                    <li><span>FAT HEAD SALAMI</span> salami with provolone &#8211; not complicated.</li>
                    <li><span>WILBUR&#8217;S HAM AND CHEESE</span> lean deli ham with provolone cheese. get this same sandwich hot if you wish.</li>
                    <li><span>DELICIOUS ROB&#8217;S ROAST BEEF</span> the highest grade thin sliced roast beef.</li>
                </ul>
                <h4>LOAD UP YOUR SANDWICH WITH YOUR CHOICE OF LETTUCE, ONION, TOMATO, PICKLE, BANANA PEPPERS, OLIVES, GIARDINERA AND JALENPENOS</h4>
                <h4>DOUBLE THE MEAT AND MAKE ANY SANDWICH JUMBO</h4>
                <h4>EXCHANGE BREAD FOR A WRAP ASK ABOUT YOUR GLUTEN FREE BREAD</h4>
            </div>
            <div class="dark-block">
                <h3>Saladwich</h3>
                <p>Your Choice. Pick any sandwich and put it on a bed of mixed greens lettuce. Add the basic trimmings that you want. Jumbo meat available&#8230;</p>
            </div>
            <div class="dark-block">
                <h3>Salads</h3>
                <ul class="menu-list">
                    <li><span>MISS GARDEN</span> crisp romaine and mixed lettuce with tomato, cheese, egg, onion, carrots.</li>
                    <li><span>MR. CHEF</span> same as the garden &#8211; add turkey and ham.</li>
                    <li><span>MRS. WALNUT</span> mixed greens, walnuts, dried cranberries and blue cheese.</li>
                    <li><span>GREEK</span>mixed lettuce, tomatoes, onions, olives, feta cheese. add gyro meet for a little extra coin.</li>
                    <li><span>COBB SALAD</span> chopped salad greens, tomato, crisp bacon, boiled, grilled or roasted chicken breast, hardboiled egg, blue cheese chunks, and red-wine vinaigrette.</li>
                </ul>
                <h4>POTATO SALAD, MACARONI ANTIPASTO SALAD, BROCCOLI SALAD, + COLE SLAW ALSO AVAILABLE</h4>
            </div>
        </div>
        <div class="col">
            <div class="dark-block">
                <h3 class="mb30">MacGenie</h3>
                <h3>MAC+cheese</h3>
                <ul class="menu-list">
                    <li><span>THE ORIGINAL GENIE</span> crumbs, sour cream, bacon.</li>
                    <li><span>NAKED GENIE</span> the above minus sour cream and bacon.</li>
                    <li><span>BUFFALO GENIE</span> crumbs, chicken, in a buffalo sauce + blue cheese.</li>
                    <li><span>MEXICAN GENIE</span> jalapeños, poblano, habanero, three cheese, chorizo + crumbs.</li>
                </ul>
                <h4>ASK ABOUT OUR GLUTEN FREE MAC</h4>
            </div>
            <div class="dark-block">
                <h3>Soups</h3>
                <ul class="menu-list">
                    <li><span>ROBBIE&#8217;S LOADED BAKED POTATO</span></li>
                    <li><span>JIM&#8217;S</span> Chili</li>
                </ul>
                <h4>PLEASE NOTE THAT OUR SOUP SPECIALS WILL VARY DAY TO DAY</h4>
            </div>
            <div class="dark-block">
                <h3>Chips</h3>
                <ul class="menu-list">
                    <li><span>DIRTY CHIPS</span> sea salt, bbq, salt + vinegar, sour cream, buffalo blue, jalapeno, salt + pepper, funky fusion, maui onion.</li>
                    <li><span>LAYS CHIPS AVAILABLE TOO</span></li>
                </ul>
            </div>
            <div class="dark-block">
                <div class="title">
                    <h3>Pizza</h3>
                    <p>AVAILABLE IN 7″, 12″, 16″</p>
                </div>
                <ul class="menu-list">
                    <li><span>FOUR CHEESE</span> a generous blend of mozzarella, provolone, cheddar and parmesan cheese seasoned with oregano.</li>
                    <li><span>DOUBLE PEPPERONI</span> juicy pepperoni and lots of it.</li>
                    <li><span>ITALIAN SAUSAGE</span> some of indy’s best fennel sausage covered with provolone and mozzarella cheese.</li>
                    <li><span>VEGGIE</span> mushrooms, onions, green peppers, black olives with provolone and mozzarella cheese.</li>
                    <li><span>BBQ</span> grilled chicken, bbq sauce, onions, jalapeños, mozzarella, provolone and cheddar cheese.</li>
                    <li><span>DELUXE</span> fennel sausage, pepperoni, ham, mushroom, green peppers, onions, olives, provolone and cheddar cheese.</li>
                    <li><span>MARGARITA</span> olive oil, tomatoes, provolone mozzarella cheese, fontina cheese, feta, cheese, fresh basil, sea salt.</li>
                    <li><span>MEXICAN STREET</span> from the real streets of Mexico that would make you crawl over the wall for- secret base sauce, chorizo, tomatoes, pico salsa, green onions, mozzarella provolone, quest alfresco cheese, cojito cheese, cabbage, lettuce and a drop of sour cream.</li>
                </ul>
                <h4>EXTRA TOPPINGS AVAILABLE FOR A FEW EXTRA COINS</h4>
                <h4>ASK ABOUT OUR 10.5&#8243; GLUTEN FREE CRUST</h4>
            </div>
            <div class="dark-block">
                <h3>Wings + Things</h3>
                <ul class="menu-list">
                    <li><span>CHICKEN WINGS</span> get an order of 5, 10, 20, or 40. available in naked and afraid, pepperoncini chipotle, mango habanero, buffalo, honey bbq, bbq kick, teritaki-sesame. blue cheese or ranch sides for a little extra coin.</li>
                    <li><span>FRIES</span> one size fits all.</li>
                    <li><span>STUFFED BREADSTICKS</span> available with fennel sausage or pepperoni.</li>
                    <li><span>BREADSTICKS</span> served with a side of jalapeno cheese.</li>
                    <li><span>BEAUTIFUL FAT PRETZEL STICKS</span> served with a side of cheese.</li>
                </ul>
            </div>
            <div class="dark-block">
                <div class="title">
                    <h3>People Sweets</h3>
                    <p>ALL SWEETS BY SCOUT&#8217;S TREAT TRUCK</p>
                </div>
                <ul class="menu-list">
                    <li><span>SEA SALT BROWNIE</span></li>
                    <li><span>COOKIE</span> choose from dark chocolate chip, oatmeal, white chocolate chip</li>
                    <li><span>CUPCAKES AS AVAILABLE</span> choose from toll house, mama cha chi choc, bday vanilla</li>
                </ul>
            </div>
            <div class="dark-block">
                <div class="title">
                    <h3>Dog Treats</h3>
                    <p>ALL TREATS BY BARK TRUCK</p>
                </div>
                <ul class="menu-list">
                    <li><span>OWEN&#8217;S PEANUT BUTTER + BACON BISCUITS</span></li>
                    <li><span>FLOSSIES CAROB COOKIES</span></li>
                    <li><span>MOOSE&#8217;S MAPLE BACON BISCUITS</span></li>
                    <li><span>BARK TRUCK CHICKEN JERKY</span></li>
                </ul>
            </div>
            <div class="dark-block">
                <div class="title">
                    <h3>Something to Drink</h3>
                    <p>JOIN US FOR BEER ON TAP + WINE IN HOUSE</p>
                </div>
                <ul class="menu-list">
                    <li><span>20 oz BOTTLE</span> coke, diet coke, pepsi, diet pepsi, mountain dew, sprite, dr. pepper, lemonade, ice tea.</li>
                    <li><span>12 oz CAN</span></li>
                    <li><span>STARBUCKS FRAPPUCCINO bottled</span> mocha, vanilla, coffee.</li>
                    <li><span>MEXICAN CANE SUGAR DRINK</span> sprite, orange, coke, mountain dew.</li>
                    <li><span>BOTTLED WATER</span></li>
                    <li><span>PERRIER</span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="text-center">
        <h3>Make people as happy as the unicorn. Feed them. We&#8217;ll help.</h3>
        <a href="/catering" class="btn">MORE ABOUT CATERING</a>
    </div>
</div>

<?php include 'parts/footer.php';?>

</body>
</html>