<?php include_once 'FormsDeliverer.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="promo-block" style="background-image: url('images/truck/promo-truck.png')">
    <span class="promo-text">Book a truck</span>
</div>

<h1><center> Find all of our trucks at <a href="https://indiefoodtrucks.com"> indiefoodtrucks.com</a> &raquo; &raquo; </center> </h1>

<?php include 'parts/footer.php';?>

</body>
</html>
