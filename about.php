<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

    <?php include 'parts/header.php';?>

    <div class="promo-block" style="background-image: url('images/about/promo-about-us.png')"></div>

    <div class="container main">
        <p>Once upon a time there were three curious people with a dream. Ray Greiner had an idea to create the best sub in all of the land and he did just that with an extraordinary and secret bread recipe. And so, Greiner’s was born. And all of the people said,  &#8220;oooh.&#8221;</p>
        <p>In another part of the land, some years later, a girl named Lisa dreamed of leaving the corporate world to become a cupcake girl and, so, she did. She bought a pink box truck, named it Scout’s and filled it with cupcakes and other nectarous things and drove all over the land of Indiana and on Georgia Street making marvelous deliciousness for all of the people from near and far. And all of the people said,  &#8220;oooooh.&#8221; Some said,  &#8220;aaaah.&#8221;</p>
        <p>Wanting that same kind of wonderful for her dog Owen and all of his canine friends from the streets of Indiana and beyond, she took a bunch more of her coins from a jar, bought another box truck and commissioned an artist named Andy Ball to put a 7 foot bone on top. And so BARK truck was born. She filled it with everyone&#8217;s favorite kibble, dog beauty supplies, flea enemy medicine, toys, bones, biscuits and beer and other house made things. From dog walking to poop scooping &#8211; every dog would be a king and every bitch would be a queen. And then this one day when the sun came out, a fairy flew in from a land afar and granted the girl a business partner named Lisa and a website that would forever be known and found as &#8211; barktruck.com. “Order here,“ they declared. The dog people heard this and they were ever so gladdened. She shouted, “We will deliver to all porches!” And all of the people all over the land said,  &#8220;yay!&#8221; and the dogs said,  &#8220;woof!&#8221;</p>
        <p>In another part of the world of Indianapolis was a noodle girl named Theresa that made the people&#8217;s dreams come true with MAC+cheese and she called it MacGenie. People lined the streets. that sometimes turned block corners, just to get the enchanting bowl of noodles. One day she decided to not work 7 days a week. The belly dancing people from another land were calling her away.</p>
        <p>Then one day the cupcake girl met a not so perfect prince on match.com , said no to a date but yes to his deli and, for a bunch more of those coins, bought Greiner’s. With the leftover coins she bought the MAC+cheese recipe from the belly dancing noodle girl and continued the wonderful dream of MacGenie MAC+cheese that still lives on today at Greiner’s.</p>
        <p>Subs and MAC, biscuits + bones, and yes, cupcakes. All under one roof and on three trucks delivered by pedal power all over downtown! The dogs barked and the people from all over the land smiled, the big kind of smile. And they all lived happily ever after.</p>
        <p>The End.</p>
        <div class="row contact-row">
            <div class="col">
                <a href="/phone">
                    <img src="images/about/phone.png" alt="">
                </a>
            </div>
            <div class="col">
                <a href="/address">
                    <img src="images/about/mail.png" alt="">
                </a>
            </div>
            <div class="col">
                <a href="/contact">
                    <img src="images/about/cimputer.png" alt="">
                </a>
            </div>
        </div>
    </div>

    <?php include 'parts/footer.php';?>

</body>
</html>