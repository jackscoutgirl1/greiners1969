<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col quick-links">
                    <h3>Quick links</h3>
                    <nav class="footer-nav">
                        <ul>
                            <li>
                                <a href="/">
                                    <img src="images/icon-home.png" alt="" />
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="/menu">
                                    <img src="images/icon-menu.png" alt="" />
                                    Menu
                                </a>
                            </li>
                            <li>
                                <a href="/about">
                                    <img src="images/icon-unicorn.png" alt="" />
                                    About
                                </a>
                            </li>
                            <li>
                                <a href="/catering">
                                    <img src="images/icon-catering.png" alt="" />
                                    Catering
                                </a>
                            </li>
                            <li>
                                <span>
                                    <img src="images/icon-truck.png" alt="" />
                                    Truck it
                                </span>
                                <ul class="sub">
                                    <li>
                                        <a href="/book-a-truck">Book a truck</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="/reviews">
                                    <img src="images/icon-microphone.png" alt="" />
                                    And all the people said...
                                </a>
                            </li>
                            <li>
                                <a href="/contact">
                                    <img src="images/icon-contact.png" alt="" />
                                    Contact
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col col-2">
                    <h3>Locations</h3>
                    <div class="row">
                        <div class="col">
                            <div class="location-block">
                                <h4>Shelby street</h4>
                                <address>
                                    <span>2126 Shelby Street</span>
                                    <span>INDIANAPOLIS, IN 46203</span>
                                    <span><a href="tel:+13177834136">317.783.4136</a></span>
                                </address>
                                <p>Monday-Saturday 10:30am to 8:00pm<br />
                                    Sunday 10:30am-6:00pm</p>
                            </div>
                            <!-- div class="location-block">
                                <h4>Nora</h4>
                                <address>
                                    <span>1738 E. 86th Street</span>
                                    <span>INDIANAPOLIS, IN 46240</span>
                                    <span><a href="tel:+13176690690">317.669.0690</a></span>
                                </address>
                                <p>Monday-Saturday 10:30am to 6:00pm<br />
                                    Closed Sundays</p>
                            </div -->
                        </div>
                        <div class="col">
                            <div class="location-block">
                                <h4>Pyramids</h4>
                                <address>
                                    <span>3500 DePauw Blvd. #3000</span>
                                    <span>INDIANAPOLIS, IN 46268</span>
                                    <span><a href="tel:+13173620055">317.362.0055</a></span>
                                </address>
                                <p>Monday-Thursday 7:30am-4:30pm<br />
                                    Friday 7:30am-3:30pm<br />
                                    Closed Weekends</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h3>Connect on social media</h3>
                    <ul class="social-list">
                        <li><a href="https://www.facebook.com/greiners1969" target="_blank"><img src="images/social-facebook.png" alt="Facebook"></a></li>
                        <li><a href="https://twitter.com/greiners1969?lang=en" target="_blank"><img src="images/social-twitter.png" alt="Twitter"></a></li>
                        <li><a href="https://www.instagram.com/greiners1969/" target="_blank"><img src="images/social-instagram.png" alt="Instagram"></a></li>
<!--                        <li><a href="https://plus.google.com/103132104742868629912" target="_blank"><img src="images/social-googleplus.png" alt="GooglePlus"></a></li>-->
                    </ul>
                    <h3>Sister companies</h3>
                    <ul class="companies-list">
                        <li>Scout&#8217;s Treat Truck</li>
                        <li>Bark Truck</li>
                    </ul>
                    <h3>Need a value?</h3>
                    <p>Reserve Our Restaurant<br />
                        CALL TODAY<br />
                        <a href="tel:+13177834136">317-783-4136</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p>&copy; <?php echo date ( 'Y' ) ; ?> Greiner's</p>
        </div>
    </div>
</footer>