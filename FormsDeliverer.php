<?php

namespace home;
include_once 'config/config.php';

use http\Message;
use Config;

class FormsDeliverer
{
    public function CheckCaptcha($response){
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';

        $config = Config::get();
        $recaptcha_secret = $config['recaptchaSecret'];

        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $response);
        $recaptcha = json_decode($recaptcha);

        // Take action based on the score returned:
        if ($recaptcha->score >= 0.5) {
            return true;

        } else {
            return false;
        }
    }

    public function SendSpreadsheet($values, $whatForm)
    {
        require __DIR__ . '/vendor/autoload.php';

        $config = Config::get();

        $client = new \Google_Client();
        $client->SetApplicationName('GS');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');
        //$client->setAuthConfig(__DIR__ . '/config/credentials.json');
        $client->setAuthConfig(__DIR__ . '/config/greeners.json');

        $service = new \Google_Service_Sheets($client);
        $spreadsheetId = $config[$whatForm]['spreadsheetId'];
        $range = $config[$whatForm]['range'];

        $body = new \Google_Service_Sheets_ValueRange(['values' => $values]);

        $params = [
            'valueInputOption' => 'RAW'
        ];

        $insert = [
            'insertDataOption' => 'INSERT_ROWS'
        ];

        $result = $service->spreadsheets_values->append(
            $spreadsheetId,
            $range,
            $body,
            $params,
            $insert
        );

        $this->sendMail($values[0], $whatForm);
    }

    protected function sendMail($values, $whatForm)
    {
        $config = Config::get();
        $to = $config['emailTo'];
        $subject = '';
        $message = '';
        switch ($whatForm) {
            case Config::contact:
                $subject = 'New Contact Form submission';
                $message = "<div>
                <p>Name: $values[0]</p>
                <p>Phone: $values[1]</p>
                <p>Email: $values[2]</p>
                <p>Interested in: $values[3]</p>
                <p>Comment: $values[4]</p>
                </div>
                ";
                break;
            case Config::bookATruck:
                $subject = 'New Book a Truck submission';
                $message = "<div>
                <p>First Name: $values[0]</p>
                <p>Last Name: $values[1]</p>
                <p>Email: $values[2]</p>
                <p>Phone: $values[3]</p>
                <p>Event Type: $values[4]</p>
                <p>Event Date: $values[5]</p>
                <p>Number of guests: $values[6]</p>
                <p>Address: $values[7]</p>
                <p>City: $values[8]</p>
                <p>State: $values[9]</p>
                <p>Zip: $values[10]</p>
                </div>
                ";

                break;
            case Config::reviews:
                $subject = 'New Review submitted';
                $message = "<div>
                <p>Client name: $values[0]</p>
                <p>Email: $values[1]</p>
                <p>Company name: $values[2]</p>
                <p>Post title: $values[3]</p>
                <p>Post content: $values[4]</p>
                <p>Rate us: $values[5]</p>
                </div>";
                break;
        }



        $client = $this->getClient();

        $service = new \Google_Service_Gmail($client);
        $mailer = $service->users_messages;

        $user = 'me';

        $strRawMessage = "From: $to<$to>\r\n";
        $strRawMessage .= "To: $to<$to>\r\n";
        $strRawMessage .= 'Subject: =?utf-8?B?' . base64_encode($subject) . "?=\r\n";
        $strRawMessage .= "MIME-Version: 1.0\r\n";
        $strRawMessage .= "Content-Type: text/html; charset=utf-8\r\n";
        $strRawMessage .= 'Content-Transfer-Encoding: quoted-printable' . "\r\n\r\n";
        $strRawMessage .= "$message\r\n";
// The message needs to be encoded in Base64URL
        $mime = rtrim(strtr(base64_encode($strRawMessage), '+/', '-_'), '=');
        $msg = new \Google_Service_Gmail_Message();
        $msg->setRaw($mime);
//The special value **me** can be used to indicate the authenticated user.
        $service->users_messages->send("me", $msg);

    }

    function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName('Gmail API PHP Quickstart');
        $client->setScopes(\Google_Service_Gmail::GMAIL_SEND);
        $client->setAuthConfig(__DIR__ . '/config/credentials_mail.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = __DIR__ . '/config/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
            //var_export('token recieved');
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            //var_export('token not recieved');
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}