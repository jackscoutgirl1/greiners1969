<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="container main">
    <div class="phone-block">
        <h2>INDY SOUTH <a href="tel:+13177834136">317.783.4136</a></h2>
        <h2>INDY NORTH <a href="tel:+13176593354">317.659.DELI</a> (3354)</h2>
    </div>
</div>

<?php include 'parts/footer.php';?>

</body>
</html>