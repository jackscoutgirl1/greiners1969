<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="promo-block" style="background-image: url('images/home/promo-home.png')">
    <span class="promo-text text-yum">Yum.</span>
</div>

<div class="container main">
    <div class="row home-links">
        <div class="col">
            <div class="block" style="background-image: url('images/home/see-menu.png')">
                <strong>see</strong>
                <a href="/menu" class="btn">Menu</a>
            </div>
        </div>
        <div class="col">
            <div class="block" style="background-image: url('images/home/try-catering.png')">
                <strong>try</strong>
                <a href="/catering" class="btn">Catering</a>
            </div>
        </div>
        <div class="col">
            <div class="block" style="background-image: url('images/home/book-trucks.png')">
                <strong>book</strong>
                <a href="/book-a-truck" class="btn">Trucks</a>
            </div>
        </div>
    </div>
    <div class="home-text">
        Once upon a time <img src="images/home/clock.png" alt="" width="59" height="64" />there was a man <img src="images/home/man.png" alt="" width="46" height="77" /> that had a dream <img src="images/home/cloud.png" alt="" width="91" height="59" /> to eat the best food <img src="images/home/sandwich.png" alt="" width="70" height="70" /> in all of the land and Indiana. <img src="images/home/indiana.png" alt="" width="44" height="68" /> So the neighbor girl that lived in the polka dot house <img src="images/home/house.png" alt="" width="54" height="69" /> right next door to the man heard <img src="images/home/ear.png" alt="" width="55" height="76" /> this and said. “Go to Greiner’s for the best subs <img src="images/home/sandwich2.png" alt="" width="78" height="59" /> and mac&amp;cheese <img src="images/home/maccheese.png" alt="" width="87" height="52" /> and your dream will come true.” He did not believe this neighborhood girl <img src="images/home/girl.png" alt="" width="79" height="68" /> and said to her, “Well I’ll be a unicorn <img src="images/home/unicorn.png" alt="" width="72" height="69" /> if they have the best food in all of the land <img  src="images/home/tree.png" alt="" width="48" height="58" /> and Indiana!” So he took his coins <img src="images/home/buttons.png" alt="" width="57" height="55" /> and drove <img src="images/home/truck.png" alt="" width="101" height="44" /> to Greiner’s where he ate the best food in all of the land and in Indiana and said, “Oh my.” And then he magically became a unicorn. <img src="images/home/personunicorn.png" alt="" width="46" height="93" /> And he lived happily ever after. He really did.
    </div>
    <div class="text-center">
        <h2>Become a Unicorn</h2>
        <a href="/menu" class="btn">Check out our great eats</a>
    </div>
</div>

<?php include 'parts/footer.php';?>

</body>
</html>