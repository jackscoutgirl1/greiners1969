<header id="header">
    <div class="container">
        <h1 id="logo">
            <a href="/">
                <img src="images/logo.png" alt="Greiner&#039;s">
            </a>
        </h1>
        <span class="btn-menu">
            <span class="material-icons">reorder</span>
        </span>
        <nav id="nav" class="clearfix">
            <ul>
                <li>
                    <a href="/">
                        <img src="images/icon-home.png" alt="" />
                        Home
                    </a>
                </li>
                <li>
                    <a href="/menu">
                        <img src="images/icon-menu.png" alt="" />
                        Menu
                    </a>
                </li>
                <li>
                    <a href="/about">
                        <img src="images/icon-unicorn.png" alt="" />
                        About
                    </a>
                </li>
                <li>
                    <a href="/catering">
                        <img src="images/icon-catering.png" alt="" />
                        Catering
                    </a>
                </li>
                <li>
                    <span>
                        <img src="images/icon-truck.png" alt="" />
                        Truck it
                    </span>
                    <ul class="sub">
                        <li>
                            <a href="/book-a-truck">Book a truck</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/reviews">
                        <img src="images/icon-microphone.png" alt="" />
                        And all the people said...
                    </a>
                </li>
                <li>
                    <a href="/contact">
                        <img src="images/icon-contact.png" alt="" />
                        Contact
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>