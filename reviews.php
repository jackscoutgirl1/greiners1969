<?php include_once 'FormsDeliverer.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="promo-block" style="background-image: url('images/reviews/promo-reviews.png')">
    <span class="promo-text">And all the people said...</span>
</div>

<div class="main">
    <div class="container">
        <div class="reviews-list">
            <div class="item">
                <strong class="heading">Good enough to feature!</strong>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Carl-Harlow.jpg" alt=""></div>
                    <div class="text">
                        <p>We featured Greiner&#8217;s in our family vlog recently. Read on.</p>
                        <p>My family and I eat at Greiner&#8217;s on Shelby St. fairly often, as it&#8217;s just around the corner from where our Friday home school group meets.</p>
                        <p>We all LOVE it, -not only the FOOD, but the atmosphere and the people who work there. Always a very tasty, very fun experience for us.</p>
                        <p>We visited The Children&#8217;s Museum recently, then headed to Greiner&#8217;s for an early dinner. It&#8217;s absolutely delicious enough to be featured, and you can take a look for yourself, here:</p>
                        <p><a href="https://www.youtube.com/watch?v=1BR0VLnCUK0">https://www.youtube.com/watch?v=1BR0VLnCUK0</a></p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Carl Harlow</strong>
                </div>
            </div>
            <div class="item">
                <strong class="heading">Patron since 1971</strong>
                <div class="holder">
                    <div class="text">
                        <p>My buddy and me once mused “If you had to eat one food item for every meal the rest of your life, what would it be?” … Hot Ham and Cheese from Greiners Sub Shop was my choice. For 48 years I have loved a Hot Ham &amp; Cheese sub. Try it my way (the old way) … Microwave the meat&amp; bun. Old school toppings. Easy on the lettuce, heavy on the onions, heavy on the Oil &amp; vinegar, extra pepper (drip the juice on the sub). I will be heading there a little later today, even though it is a 20 min. drive. I grew up in the neighborhood surrounding the Shelby Street location, and visiting the Greiners sub shop makes me feel like a kid again.</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Sammy Sai</strong>
                </div>
            </div>
            <div class="item">
                <strong class="heading">Absolutely Blown Away</strong>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Susan-Mason.jpg" alt=""></div>
                    <div class="text">
                        <p>We ate here for the first time last night. I was absolutely Blown Away by the hospitality and how friendly the staff was there. Pizza is incredible, their macaroni and cheese is to die for and their sea salt brownies are exceptional. Not many business owners get enough credit. You can tell her employees work with her not for her. Thank you for the great experience…</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Susan Mason</strong>
                </div>
            </div>
            <div class="item">
                <strong class="heading">Amazing Food. Great staff!</strong>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Jessica-Bubp.jpg" alt=""></div>
                    <div class="text">
                        <p>The food here is delicious. I would eat it every day if I could. Also, the people are great – funny, polite and passionate.</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Jessica Bubp</strong>
                </div>
            </div>
            <div class="item">
                <strong class="heading">I Was So Impressed</strong>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Becky-Malkemes.jpg" alt=""></div>
                    <div class="text">
                        <p>Had my first sub from them tonight at Fountain Square Brewery and I was so impressed! It is so hard to find a great sub like this in Indiana made with quality ingredients. I’ll definitely be eating from them again!</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Becky Malkemes</strong>
                </div>
            </div>
            <div class="item">
                <strong class="heading">Uh May Zeen!</strong>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Sunshine-Carreon.jpg" alt=""></div>
                    <div class="text">
                        <p>Me and my sista Lynn Marie met our new bestie/sista today!! Much Love Lisa!!! The decor is artsy funktastic and the food is delish!! With out a doubt I’ll be returning, my first visit was UH MAY ZEEN!!!</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Sunshine Carreon</strong>
                </div>
            </div>
            <div class="item">
                <div class="heading">First Trip, Won’t Be My Last</div>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Debbi-Sponsler.jpg" alt=""></div>
                    <div class="text">
                        <p>This was my first trip back to Greiner’s in a long time – but it will not be my last! I got the 8″ veggie sub, and it was really good. But the best part is the new MacGenie macaroni and cheese – which was honestly the best I have ever had.</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Debbi Sponsler</strong>
                </div>
            </div>
            <div class="item">
                <div class="heading">Going to Greiner’s Since the Mid 70s</div>
                <div class="holder">
                    <div class="img"><img src="images/reviews/Ernsting-Diana.jpg" alt=""></div>
                    <div class="text">
                        <p>I have been going to Greiner’s since it was on four sides of Indianapolis in the mid 70’s. I was back at the Shelby street spot with its new look just yesterday. I’m so thankful you all came through the accident and are doing ok. Will always love the food!!!</p>
                        <p>Best regards,</p>
                        <p>The pizza burger w/mushroom girl</p>
                    </div>
                </div>
                <div class="text-right">
                    <strong>Ernsting Diana</strong>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="review-form">
    <?php
    if (!empty($_POST)) {

        $deliverer = new \home\FormsDeliverer();

        $values = [
            [
                $_REQUEST['client_name'],
                $_REQUEST['email'],
                $_REQUEST['company_name'],
                $_REQUEST['post_title'],
                $_REQUEST['post_content'],
                //$_REQUEST['featured_image'],
                $_REQUEST['rate_us'],
                date("F j, Y")
            ]];

        $deliverer->SendSpreadsheet($values, 'reviews');
        ?>
        <div class="nf-form-layout">
            <p> Thank you! Your testimonial is awaiting approval.</p>
        </div>

        <?php
    } else {

    ?>

    <div class="container">
        <form id="wpmtst-submission-form" method="post" action="/reviews#sucess"
        autocomplete="off"
        >
            <h2>Tell Us What You Think</h2>
            <p class="text-sm"><span class="text-red">*</span> Required Field</p>
            <div class="form-group">
                <label class="required">Full Name</label>
                <input type="text" class="form-control" placeholder="What is your full name?" name="client_name" value=""
                       required
                       tabindex="0"
                />
            </div>
            <div class="form-group">
                <label class="required">Email</label>
                <input type="text" class="form-control" placeholder="What is your email address?" name="email" value=""
                       required
                       tabindex="0"
                />
            </div>
            <div class="form-group">
                <label>Company Name</label>
                <input type="text" class="form-control" placeholder="What is your company name?" name="company_name"
                       value=""
                       tabindex="0"
                />
            </div>
            <div class="form-group">
                <label class="required">Testimonial Title</label>
                <input type="text" class="form-control" placeholder="A headline for your testimonial."name="post_title" value=""
                       required
                       tabindex="0"
                />
            </div>
            <div class="form-group">
                <label class="required">Testimonial</label>
                <textarea cols="10" rows="5" class="form-control" name="post_content" class="" required
                          tabindex="0"
                ></textarea>
            </div>
<!--            <div class="form-group">-->
<!--                <label>Photo</label>-->
<!--                <div class="choose-file">-->
<!--                    <div class="btn-file btn">-->
<!--                        Choose file-->
<!--                        <input type="file" type="file" name="featured_image"-->
<!--                               tabindex="0"-->
<!--                        />-->
<!--                    </div>-->
<!--                    <span class="file-name"></span>-->
<!--                </div>-->
<!--                <div class="text-sm"><em>Would you like to include a photo?</em></div>-->
<!--            </div>-->
            <div class="form-group">
                <label class="required">Rate Us</label>
                <div class="rating-wrapper">
                    <div class="rating">
                        <input type="radio" id="rate_us-star1" name="rate_us" value="1" />
                        <label for="rate_us-star1" title="1 star"></label>
                        <input type="radio" id="rate_us-star2" name="rate_us" value="2" />
                        <label for="rate_us-star2" title="2 stars"></label>
                        <input type="radio" id="rate_us-star3" name="rate_us" value="3" />
                        <label for="rate_us-star3" title="3 stars"></label>
                        <input type="radio" id="rate_us-star4" name="rate_us" value="4" />
                        <label for="rate_us-star4" title="4 stars"></label>
                        <input type="radio" id="rate_us-star5" name="rate_us" value="5" checked="checked" />
                        <label for="rate_us-star5" title="5 stars"></label>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn">Add testimonial</button>
            </div>
        </form>
    </div>
        <?php
    }
    ?>

</div>

<?php include 'parts/footer.php';?>

</body>
</html>