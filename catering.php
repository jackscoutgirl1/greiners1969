<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="promo-block" style="background-image: url('images/catering/promo-catering.png')">
    <span class="promo-text">Catering</span>
</div>

<div class="container main">
    <div class="catering-items">
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinerbwsub.png" alt="">
            </div>
            <strong>Subs</strong>
        </div>
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinerpotatoesbw.png" alt="">
            </div>
            <strong>Potato Bar</strong>
        </div>
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinermexicanbw.png" alt="">
            </div>
            <strong>Mexican Cuisine</strong>
        </div>
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinersbwsoupsalad.png" alt="">
            </div>
            <strong>Soup + Salad</strong>
        </div>
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinerbwmac.png" alt="">
            </div>
            <strong>MAC + Cheese</strong>
        </div>
        <div class="item">
            <div class="img-holder">
                <img src="images/catering/greinersbwtreats.png" alt="">
            </div>
            <strong>Desserts + Candy</strong>
        </div>
    </div>
    <div class="catering-block">
        <h2>Box Lunches Starting at 9.99 Each</h2>
        <p>Includes 6 inch sandwich, chip &amp; cookie</p>
        <p>Minimum order of 4 or more. Maximum order of 1 million.</p>
    </div>
    <div class="catering-block">
        <h2>Full Event Catering</h2>
        <p>Call us or fill out the form below for a quote on your catering needs.</p>
        <p>We love 1+ day in advance, but we can handle your 911 call for emergency orders too!</p>
    </div>
    <div class="catering-phone">
        Call <a href="tel:+13177834136">317-783-4136</a>
    </div>
</div>

<?php include 'parts/footer.php';?>

</body>
</html>