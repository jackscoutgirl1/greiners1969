<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>

<div class="container main">
    <div class="row address-block">
        <div class="col">
            <h2>2126 S. Shelby St.<br />
                Indy, IN 46203</h2>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3068.037646242404!2d-86.1422357841649!3d39.73880520451211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886b5a64725be10f%3A0x11410b3db41961d1!2s2126+Shelby+St%2C+Indianapolis%2C+IN+46203!5e0!3m2!1sen!2sus!4v1502750602018" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col">
            <h2>1738 E. 86th ST.<br />
                INDY, IN 46240</h2>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3060.2642762724104!2d-86.1331093486479!3d39.913101679325905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886b52d43daefa6f%3A0x8d92ebe0b099fd41!2s1738+E+86th+St%2C+Indianapolis%2C+IN+46240!5e0!3m2!1sen!2sus!4v1519227799107" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<?php include 'parts/footer.php';?>

</body>
</html>