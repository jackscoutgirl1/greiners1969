<?php include_once 'FormsDeliverer.php'; ?>

<html lang="en-US" class="js_on  js js_active  vc_desktop  vc_transform  vc_transform">
<head>
    <?php include 'parts/head-settings.php';?>
</head>
<body>

<?php include 'parts/header.php';?>
<center><h3> Please choose one of three options below: </h3></Center>

<div class="container main">
  <center><h1> <a href="https://forms.gle/cAA3BSY8jMYRwFQ49" target=”_blank”> Tell us what you want to tell us here. &raquo; &raquo; </a></h1></center>
<br />
 <center><h1> <a href="https://forms.gle/VxNHVsFFyzyfV4Xa6" target=”_blank”> To fill our catering form touch here &raquo; &raquo; </a></h1></center>
 <br />
 <center><h1> <a href="https://forms.gle/7rpidstvkNERLX9W9" target=”_blank”> To fill our food truck request form touch here &raquo; &raquo; </a></h1></center>

</div>

<?php include 'parts/footer.php';?>

<script src="https://www.google.com/recaptcha/api.js?render=6LfZmPAUAAAAAOFxL8OMwFAtqZnVxdfMtfkHIB6w"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LfZmPAUAAAAAOFxL8OMwFAtqZnVxdfMtfkHIB6w', {action: 'contact'}).then(function(token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    });
</script>

</body>
</html>
