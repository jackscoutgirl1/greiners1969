<?php

class Config
{
    const contact = 'contact';
    const reviews = 'reviews';
    const bookATruck = 'bookATruck';
    public static function get()
    {

        /*return [
            'contact' => [
                'spreadsheetId' => '1REv7nnm6FUS5XFJmDbJe6UExVAiI6uAAGRJkvxN7ZFM',
                'range' => 'contact',
            ],
            'reviews' => [
                'spreadsheetId' => '1Yd0XJqKJvPywL1vhuoMHxuHs4WkosRhviSIP-MBoihs',
                'range' => 'review',
            ],
            'bookATruck' => [
                'spreadsheetId' => '1KVGEgmk6IorP3lBTMgjDQ-6FlWyNDF3CFkvAEvvQ4Hk',
                'range' => 'booking',
            ],
            'emailTo' => 'alexsemion@gmail.com'
        ];*/

        return [
            'contact' => [
                'spreadsheetId' => '1iOwOFb9P9dP5SzuojZvN_yCTY57wnHpXoITxVG1KBtw',
                'range' => 'contact',
            ],
            'reviews' => [
                'spreadsheetId' => '1PkjzT32srUgAs4S0qf51DLM0r1nH43vIt_FXlV4q7Xg',
                'range' => 'review',
            ],
            'bookATruck' => [
                'spreadsheetId' => '1Hg1LOWTWep_1Oom2pHGQe4XcOAhypHp8EyFi9oyOsEE',
                'range' => 'booking',
            ],
            'emailTo' => 'jackscoutgirl@gmail.com',
            'emailFrom' => 'jackscoutgirl@gmail.com',
        ];
    }

}